public class Motorcycle extends Vehicle{

    private String plate;
    private String make;
    private String model;
    private int speed;
    private int mileage;
    private boolean roadworthy;

    public Motorcycle() {
    }

    public Motorcycle(String plate, String make, String model, int speed, boolean roadworthy) {
        this.plate = plate;
        this.make = make;
        this.model = model;
        this.speed = speed;
        this.roadworthy = roadworthy;
        this.mileage = 0;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getspeed() {
        return speed;
    }

    public void setspeed(int speed) {
        this.speed = speed;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int milage) {
        this.mileage += milage;
    }

    public boolean isRoadworthy() {
        return roadworthy;
    }

    public void setRoadworthy(boolean roadworthy) {
        this.roadworthy = roadworthy;
    }


    @Override
    public String toString() {
        String state;
        if(roadworthy) {state =" Yes";}
        else{state = " NO";}

        return "Motorcycle{" +
                "plate='" + plate + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", speed=" + speed +
                ", mileage=" + mileage +
                ", roadworthy=" + state +
                '}';
    }
}
