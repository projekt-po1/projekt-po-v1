public class Truck extends Vehicle{

    private String plate;
    private String make;
    private String model;
    private int capacity;
    private int mileage;
    private boolean roadworthy;

    public Truck() {
    }

    public Truck(String plate, String make, String model, int capacity, boolean roadworthy) {
        this.plate = plate;
        this.make = make;
        this.model = model;
        this.capacity = capacity;
        this.roadworthy = roadworthy;
        this.mileage = 0;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getcapacity() {
        return capacity;
    }

    public void setcapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int milage) {
        this.mileage += milage;
    }

    public boolean isRoadworthy() {
        return roadworthy;
    }

    public void setRoadworthy(boolean roadworthy) {
        this.roadworthy = roadworthy;
    }


    @Override
    public String toString() {
        String state;
        if(roadworthy) {state =" Yes";}
        else{state = " NO";}

        return "Truck{" +
                "plate='" + plate + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", capacity=" + capacity +
                ", mileage=" + mileage +
                ", roadworthy=" + state +
                '}';
    }
}
