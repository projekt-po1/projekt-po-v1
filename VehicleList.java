import java.util.ArrayList;

public class VehicleList {
    private final ArrayList<Vehicle> vehicles = new ArrayList<>();

    public VehicleList() {}

    public VehicleList(Vehicle x) {vehicles.add(x);}

    public ArrayList<Vehicle> getVehicles() {return vehicles;}

    public void setVehicles(Vehicle x) {vehicles.add(x);}

    @Override
    public String toString() {
        return "VehicleList{" +
                "vehicles=" + vehicles +
                '}';
    }
}
