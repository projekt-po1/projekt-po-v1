public class Main {
    public static void main(String[] args) {

        Car car1 = new Car("plat123", "Renault", "Megane", 3, true);
        Truck car2 = new Truck("Plate444", "Iveco", "Daily", 200, true);
        Motorcycle car3 = new Motorcycle("Plate664", "Yamaha", "500G", 180, true);

        System.out.println(car1);
        System.out.println(car2);
        System.out.println(car3);
        VehicleList list = new VehicleList();
        list.setVehicles(car1);
        list.setVehicles(car2);
        System.out.println(list);
    }
}